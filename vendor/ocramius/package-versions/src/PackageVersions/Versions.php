<?php

declare(strict_types=1);

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    public const ROOT_PACKAGE_NAME = '__root__';
    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    public const VERSIONS          = array (
  'clue/stream-filter' => 'v1.4.0@d80fdee9b3a7e0d16fc330a22f41f3ad0eeb09d0',
  'doctrine/annotations' => 'v1.6.0@c7f2050c68a9ab0bdb0f98567ec08d80ea7d24d5',
  'doctrine/cache' => 'v1.7.1@b3217d58609e9c8e661cd41357a54d926c4a2a1a',
  'doctrine/collections' => 'v1.5.0@a01ee38fcd999f34d9bfbcee59dbda5105449cbf',
  'doctrine/common' => 'v2.8.1@f68c297ce6455e8fd794aa8ffaf9fa458f6ade66',
  'doctrine/inflector' => 'v1.3.0@5527a48b7313d15261292c149e55e26eae771b0a',
  'doctrine/lexer' => 'v1.0.1@83893c552fd2045dd78aef794c31e694c37c0b8c',
  'guzzlehttp/guzzle' => '6.3.2@68d0ea14d5a3f42a20e87632a5f84931e2709c90',
  'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646',
  'guzzlehttp/psr7' => '1.4.2@f5b8a8512e2b58b0071a7280e39f14f72e05d87c',
  'knplabs/knp-markdown-bundle' => '1.7.0@912796833a126df65ce54f81ca174474105665ea',
  'knplabs/knp-time-bundle' => 'v1.10.0@19ea7e2dee337ae98306bbdf7a4ce060f5e5d984',
  'michelf/php-markdown' => '1.8.0@01ab082b355bf188d907b9929cd99b2923053495',
  'nexylan/slack' => 'v2.0.0@884fe1db0316f042e53989cbb06f343304dec75e',
  'nexylan/slack-bundle' => 'v2.0.0@f8d0537dce96539eb94a184fa83be3070bafec2d',
  'php-http/cache-plugin' => 'v1.5.0@c573ac6ea9b4e33fad567f875b844229d18000b9',
  'php-http/client-common' => '1.7.0@9accb4a082eb06403747c0ffd444112eda41a0fd',
  'php-http/discovery' => '1.4.0@9a6cb24de552bfe1aa9d7d1569e2d49c5b169a33',
  'php-http/guzzle6-adapter' => 'v1.1.1@a56941f9dc6110409cfcddc91546ee97039277ab',
  'php-http/httplug' => 'v1.1.0@1c6381726c18579c4ca2ef1ec1498fdae8bdf018',
  'php-http/httplug-bundle' => '1.10.0@c91eef51db2c2b778946c1dd931e9aa87ac6b1e4',
  'php-http/logger-plugin' => 'v1.0.0@d6c2ac7d542bf9928a0ac7a8a249d02848b824ec',
  'php-http/message' => '1.6.0@2edd63bae5f52f79363c5f18904b05ce3a4b7253',
  'php-http/message-factory' => 'v1.0.2@a478cb11f66a6ac48d8954216cfed9aa06a501a1',
  'php-http/promise' => 'v1.0.0@dc494cdc9d7160b9a09bd5573272195242ce7980',
  'php-http/stopwatch-plugin' => '1.1.0@b9d4ab7a0f4ca1fc17e323e880d1235076e31dbf',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/log' => '1.0.2@4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'sensio/framework-extra-bundle' => 'v5.1.6@bf4940572e43af679aaa13be98f3446a1c237bd8',
  'symfony/asset' => 'v4.0.8@db6063ab6e71c0d4910328a4d10eba197e1d6b40',
  'symfony/cache' => 'v4.0.8@681c245e629409a2f1ded6bf783e833d291d8af2',
  'symfony/config' => 'v4.0.8@7c19370ab04e9ac05b74a504198e165f5ccf6dd8',
  'symfony/console' => 'v4.0.8@aad9a6fe47319f22748fd764f52d3a7ca6fa6b64',
  'symfony/debug' => 'v4.0.8@5961d02d48828671f5d8a7805e06579d692f6ede',
  'symfony/dependency-injection' => 'v4.0.8@9f1cea656afc5512c6f5e58d61fcea12acee113e',
  'symfony/event-dispatcher' => 'v4.0.8@63353a71073faf08f62caab4e6889b06a787f07b',
  'symfony/filesystem' => 'v4.0.8@5d2d655b2c72fc4d9bf7e9bf14f72a447b940f21',
  'symfony/finder' => 'v4.0.8@ca27c02b7a3fef4828c998c2ff9ba7aae1641c49',
  'symfony/flex' => 'v1.5.3@59b42aa1d3385a21683601c15e63eb35d2ebe8a4',
  'symfony/framework-bundle' => 'v4.0.8@3571d235434b566aea39d8f8bfe38860344fd9a3',
  'symfony/http-foundation' => 'v4.0.8@d0864a82e5891ab61d31eecbaa48bed5a09b8e6c',
  'symfony/http-kernel' => 'v4.0.8@6dd620d96d64456075536ffe3c6c4658dd689021',
  'symfony/lts' => 'dev-master@c1affae45b78aee036effa1759237e7fa96d4af2',
  'symfony/options-resolver' => 'v4.0.8@371532a2cfe932f7a3766dd4c45364566def1dd0',
  'symfony/polyfill-ctype' => 'v1.13.1@f8f0b461be3385e56d6de3dbb5a0df24c0c275e3',
  'symfony/polyfill-mbstring' => 'v1.7.0@78be803ce01e55d3491c1397cf1c64beb9c1b63b',
  'symfony/process' => 'v4.0.8@d7dc1ee5dfe9f732cb1bba7310f5b99f2b7a6d25',
  'symfony/routing' => 'v4.0.8@0663036dd57dbfd4e9ff29f75bbd5dd3253ebe71',
  'symfony/stopwatch' => 'v4.0.8@6795ffa2f8eebedac77f045aa62c0c10b2763042',
  'symfony/templating' => 'v4.4.1@628e5d6b9f779721a960cbc02f129c8b02c3f514',
  'symfony/translation' => 'v4.3.9@73f86a49454d9477864ccbb6c06993e24a052a48',
  'symfony/translation-contracts' => 'v1.1.7@364518c132c95642e530d9b2d217acbc2ccac3e6',
  'symfony/twig-bridge' => 'v4.0.8@7596e74f91d9c2ecb5de35811b87655e9533096f',
  'symfony/twig-bundle' => 'v4.0.8@fdaa069cd5cf3918b03d10a5e5819b064451ee4c',
  'symfony/web-server-bundle' => 'v4.0.8@20ad52df8164d2eae029e6bb24356956c52380be',
  'symfony/yaml' => 'v4.0.8@8b34ebb5989df61cbd77eff29a02c4db9ac1069c',
  'twig/twig' => 'v2.4.8@7b604c89da162034bdf4bb66310f358d313dd16d',
  'composer/ca-bundle' => '1.1.1@d2c0a83b7533d6912e8d516756ebd34f893e9169',
  'doctrine/dbal' => 'v2.7.2@c0e5736016a51b427a8cba8bc470fbea78165819',
  'doctrine/doctrine-bundle' => '1.10.3@907dafe1ba73c4c3b0f0ae8cfc1b9958c002e58c',
  'doctrine/doctrine-cache-bundle' => '1.4.0@6bee2f9b339847e8a984427353670bad4e7bdccb',
  'doctrine/doctrine-migrations-bundle' => 'v2.0.0@4c9579e0e43df1fb3f0ca29b9c20871c824fac71',
  'doctrine/instantiator' => '1.3.0@ae466f726242e637cebdd526a7d991b9433bacf1',
  'doctrine/migrations' => 'v2.0.3@0e28a16bf500d5a527eba52d53b1a65f7234a548',
  'doctrine/orm' => 'v2.6.6@2d9b9351831d1230881c52f006011cbf72fe944e',
  'easycorp/easy-log-handler' => 'v1.0.4@1a617a37ab9389eac4e2e1d14cb70ee0087d724d',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'monolog/monolog' => '1.23.0@fd8c787753b3a2ad11bc60c063cff1358a32a3b4',
  'nikic/php-parser' => 'v4.0.1@e4a54fa90a5cd8e8dd3fb4099942681731c5cdd3',
  'ocramius/package-versions' => '1.5.1@1d32342b8c1eb27353c8887c366147b4c2da673c',
  'ocramius/proxy-manager' => '2.2.3@4d154742e31c35137d5374c998e8f86b54db2e2f',
  'sensiolabs/security-checker' => 'v4.1.8@dc270d5fec418cc6ac983671dba5d80ffaffb142',
  'symfony/debug-bundle' => 'v4.0.8@3188f67995b0b54ca0600c68dac86ae822229a97',
  'symfony/doctrine-bridge' => 'v4.1.12@f1939a563057ee76c29c176676bef1d7a4d2735d',
  'symfony/dotenv' => 'v4.0.8@afb6923923e22874dac20bd042167ccb8df1d158',
  'symfony/maker-bundle' => 'v1.4.0@253cee42124de11dc0a5bb7c91e5375d7197be12',
  'symfony/monolog-bridge' => 'v4.0.8@dfd41cfdc1b0ebf1e70eec08b39423a37230c58a',
  'symfony/monolog-bundle' => 'v3.2.0@8781649349fe418d51d194f8c9d212c0b97c40dd',
  'symfony/orm-pack' => 'v1.0.7@c57f5e05232ca40626eb9fa52a32bc8565e9231c',
  'symfony/phpunit-bridge' => 'v4.0.8@e82f3f46384482f2a7dab5f00c58a36b9726bde9',
  'symfony/polyfill-php72' => 'v1.7.0@8eca20c8a369e069d4f4c2ac9895144112867422',
  'symfony/profiler-pack' => 'v1.0.3@fa2e2dad522a3bef322196abad28ffce6d0fdbc5',
  'symfony/var-dumper' => 'v4.0.8@e1b4d008100f4d203cc38b0d793ad6252d8d8af0',
  'symfony/web-profiler-bundle' => 'v4.0.8@4f6a1f77120b5e4b37c59db34a8dc0a4d3df5cf0',
  'zendframework/zend-code' => '3.4.0@46feaeecea14161734b56c1ace74f28cb329f194',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'symfony/polyfill-iconv' => '*@8d698b2822e13302fc08c52a5853de09ab47c8e4',
  'symfony/polyfill-php71' => '*@8d698b2822e13302fc08c52a5853de09ab47c8e4',
  'symfony/polyfill-php70' => '*@8d698b2822e13302fc08c52a5853de09ab47c8e4',
  'symfony/polyfill-php56' => '*@8d698b2822e13302fc08c52a5853de09ab47c8e4',
  '__root__' => 'dev-twig_extension@8d698b2822e13302fc08c52a5853de09ab47c8e4',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }
}
