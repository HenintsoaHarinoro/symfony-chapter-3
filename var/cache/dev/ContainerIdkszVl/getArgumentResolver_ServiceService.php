<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'argument_resolver.service' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentValueResolverInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentResolver/ServiceValueResolver.php';

return $this->privates['argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\ServiceLocator(array('App\\Controller\\ArticleAdminController:new' => function () {
    return ($this->privates['service_locator.H_qNGr5'] ?? $this->load('getServiceLocator_HQNGr5Service.php'));
}, 'App\\Controller\\ArticleController:show' => function () {
    return ($this->privates['service_locator.mhNPOq1'] ?? $this->load('getServiceLocator_MhNPOq1Service.php'));
}, 'App\\Controller\\ArticleController:toggleArticleHeart' => function () {
    return ($this->privates['service_locator.Wlv1VFp'] ?? $this->load('getServiceLocator_Wlv1VFpService.php'));
}, 'App\\Controller\\ArticleAdminController::new' => function () {
    return ($this->privates['service_locator.H_qNGr5'] ?? $this->load('getServiceLocator_HQNGr5Service.php'));
}, 'App\\Controller\\ArticleController::show' => function () {
    return ($this->privates['service_locator.mhNPOq1'] ?? $this->load('getServiceLocator_MhNPOq1Service.php'));
}, 'App\\Controller\\ArticleController::toggleArticleHeart' => function () {
    return ($this->privates['service_locator.Wlv1VFp'] ?? $this->load('getServiceLocator_Wlv1VFpService.php'));
})));
